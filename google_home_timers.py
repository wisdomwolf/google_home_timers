#! python3.6

import requests
from requests import ConnectionError, ConnectTimeout, ReadTimeout
from configparser import ConfigParser
import logging
from pushbullet import Pushbullet
import arrow
from json.decoder import JSONDecodeError
from urllib3.exceptions import ReadTimeoutError
from apscheduler.schedulers.background import BlockingScheduler, BackgroundScheduler
import yaml

with open('google_home_ips.yaml') as f:
    google_home_ips = yaml.load(f)

with open('new_google_home_ips.yaml') as f:
    new_google_home_ips = yaml.load(f)


logger = logging.getLogger(__name__)
config = ConfigParser()
config.read('config.ini')
PB_TOKEN = config.get('API Tokens', 'pushbullet')
pb = Pushbullet(PB_TOKEN)
scheduler = None
timers_list = []


def get_timers(home_map):
    timers = {}
    for location, address in home_map.items():
        try:
            timers[location] = requests.get(f'http://{address}:8008/setup/assistant/alarms',
            timeout=5).json().get('timer')
        except (ConnectionError, ConnectTimeout, ReadTimeoutError, ReadTimeout):
            logger.info(f"Couldn't connect to {location} at {address}")
        except JSONDecodeError:
            logger.info(f"Unable to decode JSON data from request to {location} at {address}")
    return timers


def check(scheduler):
    timers = get_timers(new_google_home_ips)
    fire_times = []
    for timer in timers.values():
        for t in timer:
            id = t.get('id')
            if id not in timers_list:
                fire_time = arrow.get(t.get('fire_time') // 1000)
                scheduler.add_job(alert, 'date', run_date=fire_time.datetime)
                timers_list.append(id)
                logger.warn(f'adding new timer {id} at {fire_time}')


def alert():
    push = pb.channels[0].push_note('Google Home', 'Timer is done!')
        

def main(interactive=False):
    logger.warning('Starting timer monitor')
    if not interactive:
        scheduler = BlockingScheduler(timezone='US/Eastern')
    else:
        scheduler = BackgroundScheduler(timezone='US/Eastern')
    scheduler.add_job(check, 'interval', seconds=10, replace_existing=True, args=(scheduler,))
    scheduler.start()
    return scheduler

if __name__ == "__main__":
    scheduler = main()

